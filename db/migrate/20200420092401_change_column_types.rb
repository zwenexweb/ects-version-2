class ChangeColumnTypes < ActiveRecord::Migration[6.0]
  def change
    change_column :documents, :reference_document_no, :string
    add_column :documents, :reference_division, :string
  end
end
