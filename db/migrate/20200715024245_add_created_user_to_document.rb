class AddCreatedUserToDocument < ActiveRecord::Migration[6.0]
  def change
    add_column :documents, :created_user, :integer
  end
end
