class CreateDocuments < ActiveRecord::Migration[6.0]
  def change
    create_table :documents do |t|
      t.date :document_date
      t.integer :document_no
      t.string :division
      t.string :sub_division
      t.integer :reference_document_no
      t.string :destination_department
      t.string :subject

      t.timestamps
    end
  end
end
