class CreateNotificationContents < ActiveRecord::Migration[6.0]
  def change
    create_table :notification_contents do |t|
      t.text :noti_message
      t.integer :details_id
      t.integer :user_id

      t.timestamps
    end
  end
end
