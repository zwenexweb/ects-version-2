class AddAdditionalsToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :phone, :string
    add_column :users, :otp, :string
    add_column :users, :otp_expires_at, :datetime
  end
end
