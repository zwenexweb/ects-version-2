class AddMobileDeviceTokenToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :mobile_device_token, :string
  end
end
