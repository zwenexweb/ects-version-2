require 'sidekiq/web'

Rails.application.routes.draw do


  resources :notification_contents
  root to: "documents#index"
  namespace :admin do
      resources :users
      # resources :documents
      resources :departments
      root to: "users#index"
      authenticate :user do
        mount Sidekiq::Web => '/sidekiq'
      end
  end
  namespace :api, constraints: { format: 'json' } do
    namespace :v1, constraints: { format: 'json' } do
      post 'users/login', to: "user#request_for_otp"
      post 'users/verify', to: "user#verify_otp"
      post 'users/update_device_token', to: "user#update_device_token"

      post 'create_comment', to: "api_v1#create_comment"
      get 'comments', to: "api_v1#comments"
      get 'notification_contents', to: "api_v1#notification_contents"
      scope :format => true, :constraints => { :format => 'json' } do
        resources :documents, only: [:index,:show]
      end
    end
  end 
  devise_for :users
  resources :documents
  post "document_create_comments", to: "documents#create_comments"
  post "update_web_push_token", to: "notifications#update_web_push_token", as: :update_device_token
  post "get_users_by_department", to: "departments#get_users_by_department", as: :get_users_by_department
  
  # notifications
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
