
// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyD0qImF6Zrx_PMyXinBseSnK95jttwvB3E",
    authDomain: "outgoing-mail-registry.firebaseapp.com",
    databaseURL: "https://outgoing-mail-registry.firebaseio.com",
    projectId: "outgoing-mail-registry",
    storageBucket: "outgoing-mail-registry.appspot.com",
    messagingSenderId: "744188574494",
    appId: "1:744188574494:web:be0147b3afe3ba2850e70f",
    measurementId: "G-KHL5XVF4V6"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

const messaging = firebase.messaging();

messaging.requestPermission().then(function() {
    messaging.getToken().then((currentToken) => {
        if (currentToken) {
            console.log(currentToken);
            sendTokenToServer(currentToken);  
            //   updateUIForPushEnabled(currentToken);  
        } else {
            // Show permission request.
            console.log('No Instance ID token available. Request permission to generate one.');
            // Show permission UI.
            //   updateUIForPushPermissionRequired();  
            //   setTokenSentToServer(false);  
        }
    }).catch((err) => {
        console.log('An error occurred while retrieving token. ', err);
        //  showToken('Error retrieving Instance ID token. ', err);  
        //  setTokenSentToServer(false);  
    });
}).catch(function(err) {
console.log('Unable to get permission to notify. ', err);
});

// Callback fired if Instance ID token is updated.
messaging.onTokenRefresh(() => {
    messaging.getToken().then((refreshedToken) => {
        console.log('Token refreshed.');
        console.log(refreshedToken);
        sendTokenToServer(refreshedToken);
    }).catch((err) => {
        console.log('Unable to retrieve refreshed token ', err);
    });
});

function sendTokenToServer(token){
    $.ajax({
        url: "/update_web_push_token",
        beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
        method: "POST",
        data: { token: token },
        success: function(response) {
            console.log(response, "SEND TOKEN TO SERVER");
        }   
    });
}
