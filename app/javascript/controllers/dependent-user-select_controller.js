import { Controller } from 'stimulus'

export default class extends Controller {

    static targets = ["departmentSelect", "userSelect"]

    connect() {
        console.log('connected to user select controller');
        console.log(this.data.get('documentId'));
    }

    handleSelectChange(event) {
        let departments = this.getSelectedValues(event);
        this.updateUserSelect(departments);
    }

    getSelectedValues(event){
        return [...event.target.selectedOptions].map(option => option.value);
    }

    updateUserSelect(departments){
        this.clearUserSelect();
        console.log(departments);
        $.ajax({
            url: "/get_users_by_department",
            context: this,
            beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
            method: "POST",
            data: { departments: departments, document_id:  this.data.get('documentId')},
            success: function(response) {
                    console.log(response);
                    const users = response.users;
                    const authorized_users = response.authorized_users;
                    const userSelectBox = this.userSelectTarget;

                    if(this.data.get('documentId') == 0){
                        users.forEach(user => {
                            const opt = document.createElement('option');
                            opt.value = user[0];
                            opt.innerHTML = user[1];
                            opt.selected = true;
                            userSelectBox.appendChild(opt);
                        })
                    }else{
                        users.forEach(user => {
                            const opt = document.createElement('option');
                            opt.value = user[0];
                            opt.innerHTML = user[1];
                            if(authorized_users.includes(user[0])){
                                opt.selected = true;
                            }else{
                                opt.selected = false; 
                            }
                            userSelectBox.appendChild(opt);
                        })
                    }
            }
        });
    }

    clearUserSelect(){
        this.userSelectTarget.innerHTML = "";
    }

    disconnect(){			
    }
}

