import { Controller } from 'stimulus'



export default class extends Controller {

    connect() {
	document.getElementById("myGrid").innerHTML=""
	agGrid.LicenseManager.setLicenseKey("Zwenexsys_MultiApp_1Devs16_October_2020__MTYwMjgwMjgwMDAwMA==5c5e3dfffe945cb3b7229d4ba32bda30");



	var columnDefs = [
	    {headerName: "အခြေခံပညာဦးစီးဌာန၊ တင်ပြစာ၊ ညှိနှိုင်းစာ၊ အကြောင်းကြားစာများ", children: [
		{headerName: "", field: "attributes.html.actions", cellRenderer: function(param){return param.value}, hide: true},
		{headerName: "စဥ်", field: "attributes.html.id",  filter: 'agNumberColumnFilter', hide: true},
		{headerName: "စာအမှတ်", field: "attributes.document_no",  filter: 'agTextColumnFilter',cellStyle: {'font-weight': 'bold'}, minWidth: 120, maxWidth: 120,
		cellRenderer: function(params) {
			return '<a href="/documents/'+params.data.id+'" rel="noopener" class="text-primary">'+ params.value+'</a>'
		}},
		{headerName: "ရက်စွဲ", field: "attributes.html.document_date",  filter: 'agTextColumnFilter', minWidth: 120, maxWidth: 120},
		{headerName: "ဌာနခွဲ", field: "attributes.sub_division",  filter: 'agTextColumnFilter', minWidth: 120, maxWidth: 120},
		{headerName: "ပေးပို့မည့်ဌာန", field: "attributes.destination_department",  filter: 'agTextColumnFilter', minWidth: 120, maxWidth: 200},
		{headerName: "အကြောင်းအရာ", field: "attributes.subject",minWidth: 300, maxWidth: 600, cellRenderer: function(param){return param.value},cellStyle: {'font-weight': 'bold'}},
				{ headerName: "ဖိုင်အမည်", field: "attributes.division",  filter: 'agTextColumnFilter',minWidth: 130, maxWidth: 200},
		{headerName: "ရည်ညွှန်းစာအမှတ်", field: "attributes.reference_document_no",  filter: 'agTextColumnFilter',minWidth: 130, maxWidth: 200},
		{headerName: "ရည်ညွှန်းစာဌာန", field: "attributes.reference_division",  filter: 'agTextColumnFilter',minWidth: 130, maxWidth: 200},
		{headerName: "ဆောင်ရွက်နေသည့်အခြေအနေ", field: "attributes.working_status",  filter: 'agTextColumnFilter',minWidth: 130, maxWidth: 200}
	    ]}
	];


	// specify the data
	var rowData = this.documents.data;

	// let the grid know which columns and what data to use
	var gridOptions = {
	    columnDefs: columnDefs,
	    rowData: rowData,	   
	    sideBar: true,
	    defaultColDef: {
		flex: 1,
		// minWidth: 62,
		filter: true,
		autoHeight: true,
		// sortable: true,
		resizable: true,
	    },
	    floatingFilter: true	   

	};

	

	var eGridDiv = document.querySelector('#myGrid');

	$('#myGrid').height((window.innerHeight - 75 - 25) + "px");

	gridOptions.rowStyle = { background: 'white' };

	// all even rows assigned 'my-shaded-effect'
	gridOptions.getRowStyle = function(params) {
		// console.log(params);
		if (params.node.rowIndex % 2 === 0) {
			return { background: '#deebf7' };
		}
	}

	var grid = new agGrid.Grid(eGridDiv, gridOptions);

	// gridOptions.api.setDomLayout('autoHeight');
	gridOptions.api.doLayout();

    }
    disconnect(){			

    }
    get documents() {
	// console.log(this.data.get("documents"));
	console.log(this.data);
	return JSON.parse(this.data.get("documents"))
    }
}

