class NotificationContentSerializer
    include FastJsonapi::ObjectSerializer

    attributes :id, :noti_message, :details_id, :user_ids

    attribute :created_at do |object|
      object.created_at.strftime("%d-%m-%Y")
    end

    
end 