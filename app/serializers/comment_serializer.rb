class CommentSerializer
    include FastJsonapi::ObjectSerializer

    attributes :id, :title, :comment, :user_id
end 