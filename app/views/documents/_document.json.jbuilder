json.extract! document, :id, :document_date, :document_no, :division, :sub_division, :reference_document_no, :destination_department, :subject, :created_at, :updated_at
json.url document_url(document, format: :json)
