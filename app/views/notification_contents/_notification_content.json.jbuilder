json.extract! notification_content, :id, :noti_message, :details_id, :user_id, :created_at, :updated_at
json.url notification_content_url(notification_content, format: :json)
