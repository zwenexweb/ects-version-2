class ApplicationController < ActionController::Base


  def mobile_device?
    if params[:is_mobile]
      params[:is_mobile] == "1"
    elsif session[:is_mobile] 
      session[:is_mobile] == "1"      
    else
      request.user_agent =~ /Mobile|webOS/
    end
  end

  helper_method :mobile_device?

end
