class Api::V1::DocumentsController < Api::V1::ApiV1Controller
  
  
  before_action :set_user, :set_headers, :except => [:request_for_otp, :verify_otp]
  
  before_action :set_api_v1_document, only: [:show, :edit, :update, :destroy]
  before_action :check_authorized_editor, only: [:show]

  # GET /api/v1/documents
  # GET /api/v1/documents.json
  def index
    @api_v1_documents = Document.all
  end

  # GET /api/v1/documents/1
  # GET /api/v1/documents/1.json
  def show
  end

  # GET /api/v1/documents/new
  def new
    @api_v1_document = Document.new
  end

  # GET /api/v1/documents/1/edit
  def edit
  end

  # POST /api/v1/documents
  # POST /api/v1/documents.json
  def create
    @api_v1_document = Document.new(api_v1_document_params)

    respond_to do |format|
      if @api_v1_document.save
        format.html { redirect_to @api_v1_document, notice: 'Document was successfully created.' }
        format.json { render :show, status: :created, location: @api_v1_document }
      else
        format.html { render :new }
        format.json { render json: @api_v1_document.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /api/v1/documents/1
  # PATCH/PUT /api/v1/documents/1.json
  def update
    respond_to do |format|
      if @api_v1_document.update(api_v1_document_params)
        format.html { redirect_to @api_v1_document, notice: 'Document was successfully updated.' }
        format.json { render :show, status: :ok, location: @api_v1_document }
      else
        format.html { render :edit }
        format.json { render json: @api_v1_document.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /api/v1/documents/1
  # DELETE /api/v1/documents/1.json
  def destroy
    @api_v1_document.destroy
    respond_to do |format|
      format.html { redirect_to api_v1_documents_url, notice: 'Document was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_api_v1_document
      @api_v1_document = Document.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def api_v1_document_params
      params.fetch(:api_v1_document, {})
    end

    def check_authorized_editor
      if params[:id].present?
        if @current_user.is_normal_user? && !@api_v1_document.user_ids.include?(@current_user.id)
          render :json => failureJson(jresponse(:bad_request),nil, "You are not authorized.") and return
        end
      else
        render :json => failureJson(jresponse(:bad_request),nil, "You are not authorized.") and return
      end
    end 
end
