# coding: utf-8
class Api::V1::ApiV1Controller < ApplicationController
  include NotificationSender
  skip_before_action :verify_authenticity_token
  skip_before_action :authenticate_user!, raise: false
  before_action :set_user, :set_headers, :except => [:request_for_otp, :verify_otp]
  before_action :check_authorized_editor, only: [:create_comment, :comments]

  @current_user = nil
  $RESPONSES = {
    :success => "Success",
    :sent_otp => "OTP was sent to your phone.",
    :sent_otp_mm => "သင့်ဖုန်းနံပါတ်သို့တစ်ခါသုံးလျှို့ဝှက်ကုဒ်ပေးပို့လိုက်ပါပြီ",
    :user_not_found => "User not found",
    :user_not_found_mm => "ဤဖုန်းနံပါတ်သည် C4M System တွင်စာရင်းပေးသွင်းထားခြင်းမရှိသောကြောင့် ဝင်ရောက်အသုံးပြုခွင့်မရှိပါ။သက်ဆိုင်ရာမြို့နယ်ရုံးများတွင် စာရင်းပေးသွင်းရန်လိုအပ်ပါသည်။",
    :query_invalid => "Query is invalid",
    :client_not_found => "Client not found",
    :otp_is_invalid => "OTP is Invalid",
    :otp_is_invalid_mm => "တစ်ခါသုံးလျှို့ဝှက်ကုဒ် မှားယွင်းနေပါသည်။",
    :form_list => "Form list retrieved",
    :form_list_mm => "လျှောက်လွှာစာရင်း ရယူပြီးပါပြီ။",
    :form_is_invalid => "Form is invalid",
    :form_is_invalid_mm => "လျှောက်လွှာ မမှန်ကန်ပါ။",
    :date_missing => "Date is missing in form",
    :date_missing_mm => "လျှောက်လွှာ၏အညွှန်းရှိ ခုနှစ်(သို့)လ မပါဝင်ပါ။",
    :date_is_invalid => "Date is invalid",
    :date_is_invalid_mm => "လျှောက်လွှာ၏အညွှန်းရှိ ခုနှစ်(သို့)လ မမှန်ကန်ပါ။",
    :form_code_required => "Form code is missing",
    :form_code_required_mm => "လျှောက်လွှာ၏ကုဒ်နံပါတ် မပါဝင်ပါ။",
    :form_is_already_saved => "Form is already saved.",
    :form_is_already_saved_mm => "ယနေ့  အတွက် သတင်းပို့ပြီးဖြစ်ပါသည်။",
    :invalid_date => "Invalid date",
    :invalid_date_mm => "နေ့စွဲ မမှန်ကန်ပါ။",
    :max_count => "Count must be less than 100",
    :not_found => "404",
    :bad_request => "Bad Request",
    :create_comment => "Comment was successfully created.",
    :create_comment_mm => "Comment တည်ဆောက်ခြင်း အောင်မြင်ပါသည်။",
    :create_comment_error => "Comment was not created.",
    :create_comment_error_mm => "Comment တည်ဆောက်ခြင်း မအောင်မြင်ပါ။",
    :create_comment_requirement => "Document id, title and comment is required to create comment.",
    :create_comment_requirement_mm => "Comment တည်ဆောက်ရန် document id, comment ခေါင်းစဥ်နှင့်စာ ပါရန်လိုအပ်ပါသည်။"
  }
  # API Response JSON Definitions
  def defaultJson(success,message,message_mm,body) return {success: success, message: message, message_mm: message_mm, body: body} end
  def successJson(message,message_mm,body = nil) return defaultJson(true,message,message_mm,body) end
  def failureJson(message,message_mm,body = nil) return defaultJson(false,message,message_mm,body) end
  def jresponse(code) return $RESPONSES[code] end

  def reformat_json(key,body)
    puts body
    body["#{key}"] = body.delete(:data)
    return body
  end

  def get_school
  end

  def request_for_otp
    @phone = params[:phone]
    user = User.find_by(phone: @phone)
    if !user.nil? and user.valid?
      user.send_otp
      render :json => successJson(jresponse(:sent_otp),jresponse(:sent_otp_mm)).to_json and return
    end
    render :json => failureJson(jresponse(:user_not_found),jresponse(:user_not_found_mm)).to_json and return
  end

  def verify_otp
    @phone = params[:phone]
    @otp = params[:otp]
    if User.exists? phone: @phone
      @user = User.find_by_phone(@phone)
      p @user.errors
      if @user.valid_otp? @otp.to_s
        @user.update(mobile_device_token: params[:device_token])
        @userJson = AuthUserSerializer.new(@user)
        render :json => successJson(jresponse(:success),nil,@userJson) and return
      end
      render :json => failureJson(jresponse(:otp_is_invalid),jresponse(:otp_is_invalid_mm)) and return
    end
    render :json => failureJson(jresponse(:user_not_found),jresponse(:user_not_found_mm)) and return
  end

  def destroy
    current_user = User.find_by(api_token: request.headers["X-Auth-Token"])
    current_user.logout
    render :json => { msg: "Success"}
  end

  def update_device_token
    if params[:device_token].present?
      @current_user.update(mobile_device_token: params[:device_token])
      render :json => successJson(jresponse(:success),nil,"Updating device token is successful.") and return
    else 
      render :json => failureJson(jresponse(:bad_request),nil, "Device token is required") and return
    end 
  end



  def set_user
    authToken = request.headers["X-Auth-Token"]
    if authToken.nil?
      render :json => failureJson(jresponse(:user_not_found),jresponse(:user_not_found_mm)) and return
    end
    @current_user = User.find_by_api_token(authToken)
    if @current_user.nil?
      render :json => failureJson(jresponse(:user_not_found),jresponse(:user_not_found_mm)) and return
    end
  end

  def create_comment
    if params[:title].present? && params[:comment].present? && params[:document_id]
      @document = Document.find(params[:document_id])
      comment = @document.comments.create
  
      comment.title = params[:title]
      comment.comment = params[:comment]
      comment.user_id = @current_user.id
      if comment.save
        # mobile_registration_ids = User.get_admin().pluck(:mobile_device_token) + (@document.users.pluck(:mobile_device_token) - [@current_user.mobile_device_token])
        # web_push_registration_ids = User.get_admin().pluck(:web_push_token) + (@document.users.pluck(:web_push_token) - [@current_user.web_push_token])

        if @document.comments.map{|x| User.find(x.user_id).user_type}.include?("admin")
          admin_mobile_registration_ids = []
          @document.comments.map{|x| admin_mobile_registration_ids << User.find(x.user_id) if User.find(x.user_id).user_type == "admin" && User.find(x.user_id) != current_user.id}

          mobile_registration_ids = admin_mobile_registration_ids.uniq.pluck(:mobile_device_token) + (@document.users.pluck(:mobile_device_token) - [current_user.mobile_device_token])
          web_push_registration_ids = admin_mobile_registration_ids.uniq.pluck(:web_push_token) + (@document.users.pluck(:web_push_token) - [current_user.web_push_token])
        else
          mobile_registration_ids = (@document.users.pluck(:mobile_device_token) - [current_user.mobile_device_token])
          web_push_registration_ids = (@document.users.pluck(:web_push_token) - [current_user.web_push_token])
        end
        
        message = "#{@current_user.name} commented on document number #{@document.document_no}"

        send_fcm_push_notification(message, mobile_registration_ids, @document.id, 'comment')
        send_fcm_push_notification(message, web_push_registration_ids, @document.id, 'comment')
        
        @notification_content = NotificationContent.new
        @notification_content.noti_message = message
        @notification_content.details_id = @document.id 
        # @notification_content.user_ids = @notification_content.user_ids << [@document.created_user] - [@current_user.id]
        @notification_content.user_ids << @document.created_user
        @notification_content.user_ids << @document.comments.last(2).first.user_id
        @notification_content.user_ids = @notification_content.user_ids - [@current_user.id]
        @notification_content.save
        render :json => successJson(jresponse(:create_comment),jresponse(:create_comment_mm)).to_json and return
      else 
        render :json => failureJson(jresponse(:create_comment_error),jresponse(:create_comment_error_mm)) and return
      end 
    else 
      render :json => failureJson(jresponse(:create_comment_requirement),jresponse(:create_comment_requirement_mm)) and return
    end 
  end

  def comments
    if params[:document_id]
      @document = Document.find(params[:document_id])
      @comments = @document.comments.map{|x| CommentSerializer.new(x)}

      render :json => successJson(jresponse(:success),nil, @comments).to_json and return
    end
  end

  def notification_contents
    # @notification_contents = NotificationContent.where(user_id: nil).or(NotificationContent.where(user_id: current_user.id)).order("created_at DESC").map{|x| NotificationContentSerializer.new(x)}
    @notification_contents = NotificationContent.where("#{@current_user.id}=ANY(user_ids)").order("created_at DESC").map{|x| NotificationContentSerializer.new(x)}
    render :json => successJson(jresponse(:success),nil, @notification_contents).to_json and return
  end 

  private


  def set_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Expose-Headers'] = 'ETag'
    headers['Access-Control-Allow-Methods'] = 'GET, POST, PATCH, PUT, DELETE, OPTIONS, HEAD'
    headers['Access-Control-Allow-Headers'] = '*,x-requested-with,school-id,Content-Type,If-Modified-Since,If-None-Match'
    headers['Access-Control-Max-Age'] = '86400'
  end

  def check_authorized_editor
    if params[:document_id].present?
      @document = Document.find(params[:document_id])
      if @current_user.is_normal_user? && !@document.user_ids.include?(@current_user.id)
        render :json => failureJson(jresponse(:bad_request),nil, "You are not authorized.") and return
      end
    else
      render :json => failureJson(jresponse(:bad_request),nil, "You are not authorized.") and return
    end
  end 

end
