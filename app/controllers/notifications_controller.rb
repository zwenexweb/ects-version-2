class NotificationsController < ApplicationController
    before_action :authenticate_user!
    
    def update_web_push_token
        if params[:token].present?
            if current_user.update(web_push_token: params[:token])
                render json: {status_code: "success" }.to_json, status: 200
            else 
                render json: {status_code: "failure" }.to_json, status: 200
            end 
        else 
            render json: {status_code: "failure" }.to_json, status: 200
        end 
    end 

end 