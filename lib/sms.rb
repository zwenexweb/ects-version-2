require 'twilio-ruby'
require 'uri'
require 'net/http'
require 'net/https'

module Sms

  def request_otp to,otp
    sms_prefix = "#{otp} is C4M Password."
    send_sms(to,sms_prefix) 
  end

  def generate_otp
    unless Rails.env.development?
      return rand(99999).to_s.center(5, rand(9).to_s)
    end
    return "66666"
  end

  def send_sms to,body
    unless Rails.env.development?
      send_poh(to,body)
#      @client.api.account.messages.create(
#        from: @from,
#        to: to,
#        body: body
#      )
    end
  end


  def send_poh to,body
    url = URI("https://smspoh.com/api/v2/send")

    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true

    request = Net::HTTP::Post.new(url)
    request["Content-Type"] = 'application/json'
    request["Authorization"] = 'Bearer hQYTBB1ioOPDLJdoHW19GMwW54uzlWK9N0cb7bWtz2snooNCMAdCcaN3y8lXzevb'
    request.body = { to: to,
                     message: body,
                     sender: "C4MAPP"}.to_json.to_s
    puts request.body
    response = http.request(request)
    puts response.read_body
  end
end
